## Requerimientos:
Python 3.8 en adelante

## Dependencias:
- pandas
- snscrape

## Instalación:

```bash
pip install pandas snscrape
```